package be.personify.iam.api;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import be.personify.iam.api.util.Criterium;
import be.personify.iam.api.util.CriteriumOperator;
import be.personify.iam.api.util.Query;
import be.personify.iam.api.util.QueryOperator;
import be.personify.iam.api.util.QueryParser;

public class QueryParserTest {
	
	private static final String TEST_SIMPLE_EQUALS = "(test=true)";
	private static final String TEST_SIMPLE_NOT_EQUALS = "(test!=true)";
	private static final String TEST_SIMPLE_AND = "(test=true) AND (toast=false)";
	private static final String TEST_SIMPLE_OR = "(test=true) OR (toast=false)";
	private static final String TEST_AND_OR = "(test=true) AND ((test=false) OR (test=both))";
	
	private static final String TEST_WRONG_AND = "(test=true) AND (toast=false) OR (sjaka=maka)";
	
	private QueryParser parser = new QueryParser();
	
	
	@Test
	public void testSimpleEquals() {
		Query query = parser.parse(TEST_SIMPLE_EQUALS);
		assertNotNull(query);
		assertTrue(query.getOperator() == QueryOperator.AND);
		assertTrue(query.getQueries().isEmpty());
		assertTrue(query.getCriteria().size() == 1);
		Criterium criterium = query.getCriteria().get(0);
		assertTrue(criterium.getKey().equals("test"));
		assertTrue(criterium.getValue().equals("true"));
		assertTrue(criterium.getOperator() == CriteriumOperator.EQUALS);
	}
	
	@Test
	public void testSimpleNotEquals() {
		Query query = parser.parse(TEST_SIMPLE_NOT_EQUALS);
		assertNotNull(query);
		assertTrue(query.getOperator() == QueryOperator.AND);
		assertTrue(query.getQueries().isEmpty());
		assertTrue(query.getCriteria().size() == 1);
		Criterium criterium = query.getCriteria().get(0);
		assertTrue(criterium.getKey().equals("test"));
		assertTrue(criterium.getValue().equals("true"));
		assertTrue(criterium.getOperator() == CriteriumOperator.NOT_EQUALS);
	}
	
	@Test
	public void testSimpleAnd() {
		Query query = parser.parse(TEST_SIMPLE_AND);
		assertNotNull(query);
		assertTrue(query.getOperator() == QueryOperator.AND);
		assertTrue(query.getQueries().isEmpty());
		assertTrue(query.getCriteria().size() == 2);
		
		Criterium criteriumOne = query.getCriteria().get(0);
		assertTrue(criteriumOne.getKey().equals("test"));
		assertTrue(criteriumOne.getValue().equals("true"));
		assertTrue(criteriumOne.getOperator() == CriteriumOperator.EQUALS);
		
		Criterium criteriumTwo = query.getCriteria().get(1);
		assertTrue(criteriumTwo.getKey().equals("toast"));
		assertTrue(criteriumTwo.getValue().equals("false"));
		assertTrue(criteriumTwo.getOperator() == CriteriumOperator.EQUALS);
	}
	
	@Test
	public void testSimpleOr() {
		Query query = parser.parse(TEST_SIMPLE_OR);
		assertNotNull(query);
		assertTrue(query.getOperator() == QueryOperator.OR);
		assertTrue(query.getQueries().isEmpty());
		assertTrue(query.getCriteria().size() == 2);
		
		Criterium criteriumOne = query.getCriteria().get(0);
		assertTrue(criteriumOne.getKey().equals("test"));
		assertTrue(criteriumOne.getValue().equals("true"));
		assertTrue(criteriumOne.getOperator() == CriteriumOperator.EQUALS);
		
		Criterium criteriumTwo = query.getCriteria().get(1);
		assertTrue(criteriumTwo.getKey().equals("toast"));
		assertTrue(criteriumTwo.getValue().equals("false"));
		assertTrue(criteriumTwo.getOperator() == CriteriumOperator.EQUALS);
	}
	
	
	@Test
	public void testAndOr() {
		Query query = parser.parse(TEST_AND_OR);
		assertNotNull(query);
		assertTrue(query.getOperator() == QueryOperator.AND);
		assertTrue(query.getQueries().size() == 1);
		assertTrue(query.getCriteria().size() == 1);
		
		Criterium criteriumOne = query.getCriteria().get(0);
		assertTrue(criteriumOne.getKey().equals("test"));
		assertTrue(criteriumOne.getValue().equals("true"));
		assertTrue(criteriumOne.getOperator() == CriteriumOperator.EQUALS);
		
		Query q = query.getQueries().get(0);
		assertTrue(q.getOperator() == QueryOperator.OR);
		assertTrue(q.getQueries().size() == 0);
		assertTrue(q.getCriteria().size() == 2);
		
	}
	
	
	
	@Test( expected = RuntimeException.class)
	public void testWrong() {
		Query query = parser.parse(TEST_WRONG_AND);
		System.out.println(query);
	}

}

package be.personify.iam.api.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryParser {
	
	private static final String REGEX = "(?=\\()(?:(?=.*?\\((?!.*?\\1)(.*\\)(?!.*\\2).*))(?=.*?\\)(?!.*?\\2)(.*)).)+?.*?(?=\\1)[^(]*(?=\\2$)";
	
	private static final Pattern p = Pattern.compile(REGEX);
	
	

	/**
	 * Parses the string expression to a query		
	 * @param expression the expression
	 * @return a Query
	 */
	public Query parse(String expression) {
		
		//filter
		expression = filter(expression);
		
		Query query = new Query();
		Matcher m = p.matcher(expression);
		
		int previousEnd = 0;
		int count = 0;
		while (m.find()) {
			String condition = m.group();
			if ( previousEnd != 0 ) {
				String operatorString = expression.substring(previousEnd, m.start());
				QueryOperator queryOperator = QueryOperator.valueOf(operatorString);
				if ( count > 1 && queryOperator != query.getOperator()) {
					throw new RuntimeException("OR and AND can not be combined in the same expression, use round brackets -> (test=true) AND ((toast=false) OR (taste=no))");
				}
				else {
					query.setOperator(queryOperator);
				}
			}
			previousEnd = m.end();
            String qr = condition.substring(1, condition.length() -1);
            if ( condition.startsWith("((")) {
            	query.addQuery(parse(qr));
            }
            else {
            	Criterium criterium = stringToCriterium(qr);
            	query.addCriterium(criterium);
            }
            count = count + 1;
        }
		
		return query;
	}
	
	
	
	private Criterium stringToCriterium(String expression) {
		for ( CriteriumOperator operator : CriteriumOperator.values()) {
			if ( expression.contains(operator.getValue())) {
				Criterium criterium = new Criterium();
				criterium.setOperator(operator);
				String[] splitted = expression.split(operator.getValue());
				if ( operator.getNumberOfParts() == splitted.length ) {
					criterium.setKey(splitted[0]);
					if ( operator.getNumberOfParts() > 1 ) {
						criterium.setValue(splitted[1]);
					}
					return criterium;
				}
				else {
					throw new RuntimeException("the expression [" + expression + "] can not be splitted correctly, expexted number of parts [" + operator.getNumberOfParts() + "]");
				}
			}
		}
		throw new RuntimeException("the expression [" + expression + "] does not contain a valid operator!");
	}



	
	private String filter( String s ) {
		s = s.replaceAll(" \\(", "(");
		s = s.replaceAll("\\( ", "(");
		s = s.replaceAll(" \\)", ")");
		s = s.replaceAll("\\) ", ")");
		s = s.replaceAll(" AND", "AND");
		s = s.replaceAll("AND ", "AND");
		s = s.replaceAll(" OR", "OR");
		s = s.replaceAll("OR ", "OR");
		return s;
	}
	

}

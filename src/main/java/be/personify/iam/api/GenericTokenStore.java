package be.personify.iam.api;

import be.personify.iam.api.util.TokenResponse;

public interface GenericTokenStore {
	
	public TokenResponse getTokenResponse();

}

package be.personify.iam.api.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel.PageMetadata;


public class SearchResult extends HashMap<String,Object> {
	
	
	private static final long serialVersionUID = 1L;


	public SearchResult( String type, PageMetadata m, List<Object> entities, Link ... links ) {
		super();
		put(HateosUtil.PAGE, m);
		HateosUtil.addLinks(this,links);
		Map<String,Object> e = new HashMap<String,Object>();
		e.put(type,entities);
		put(HateosUtil.EMBEDDED, e);
	}


	
	
	
	public SearchResult( String type, List<Object> entities, Link ... links ) {
		super();
		HateosUtil.addLinks(this,links);
		Map<String,Object> e = new HashMap<String,Object>();
		e.put(type,entities);
		put(HateosUtil.EMBEDDED, e);
	}
	
	
	public SearchResult( Map<String,Object> entity, Link ... links ) {
		super();
		HateosUtil.addLinks(this,links);
		for (String key : entity.keySet() ) {
			put(key, entity.get(key));	
		}
		//put("_embedded", e);
	}
	
	

}

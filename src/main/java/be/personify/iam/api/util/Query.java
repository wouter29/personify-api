package be.personify.iam.api.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Query class to perform queries
 * @author vanderw
 *
 */
public class Query implements Serializable {
	
	private static final long serialVersionUID = 6020373022517713585L;

	
	private List<Query> queries = new ArrayList<Query>();
	private List<Criterium> criteria = new ArrayList<Criterium>();
	
	private QueryOperator operator = QueryOperator.AND;
	
	
	public Query() {}

	
	public Query( Criterium...criteria ) {
		operator = QueryOperator.AND;
		for ( Criterium sc : criteria ) {
			this.criteria.add(sc);
		}
	}
	
	public Query( QueryOperator operator, Criterium...criteria ) {
		this.operator = operator;
		for ( Criterium sc : criteria ) {
			this.criteria.add(sc);
		}
	}
	
	public List<Query> getQueries() {
		return queries;
	}


	public void setQueries(List<Query> queries) {
		this.queries = queries;
	}


	public List<Criterium> getCriteria() {
		return criteria;
	}


	public void setCriteria(List<Criterium> criteria) {
		this.criteria = criteria;
	}


	public QueryOperator getOperator() {
		return operator;
	}


	public void setOperator(QueryOperator operator) {
		this.operator = operator;
	}


	public Query( QueryOperator operator, Query...queries ) {
		this.operator = operator;
		for ( Query q : queries ) {
			this.queries.add(q);
		}
	}
	
	
	
	public void addCriterium( Criterium criterium ) {
		criteria.add(criterium);
	}
	
	public void addQuery( Query query ) {
		queries.add(query);
	}
	
	
	public boolean isEmpty() {
		return queries.isEmpty() && criteria.isEmpty();
	}


	@Override
	public String toString() {
		return "Query [queries=" + queries + ", criteria=" + criteria + ", operator=" + operator + "]";
	}
	
	
	
	
	
	
	
	
	
	
}

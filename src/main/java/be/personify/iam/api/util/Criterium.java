package be.personify.iam.api.util;

import java.io.Serializable;
import java.util.Date;

import be.personify.util.DateUtils;

/**
 * A simple search criterium class
 * @author vanderw
 *
 */
public class Criterium implements Serializable {

	private static final long serialVersionUID = -3184919196138992744L;

	private String key;

	private Object value;
	
	private CriteriumOperator operator;
	
	public Criterium() {}
	
	/** 
	 * Constructor
	 * @param key the key
	 * @param value the value
	 */
	public Criterium(String key, Object value) {
		this.key = key;
		this.value = value;
		this.operator = CriteriumOperator.EQUALS;
	}
	
	
	public Criterium(String key, Date value, CriteriumOperator operator) {
		this.key = key;
		this.value = DateUtils.DATE_FORMAT.format(value);
		this.operator = operator;
	}
	
	
	/**
	 * Cosntructor
	 * @param key the key
	 * @param value the value
	 * @param operator the operator
	 */
	public Criterium(String key, Object value, CriteriumOperator operator) {
		this.key = key;
		this.value = value;
		this.operator = operator;
	}
	
	

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public CriteriumOperator getOperator() {
		return operator;
	}

	public void setOperator(CriteriumOperator operator) {
		this.operator = operator;
	}

	@Override
	public String toString() {
		return "SearchCriterium [key=" + key + ", value=" + value + ", operator=" + operator + "]";
	}
	
	
	
	
}

package be.personify.iam.api.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;

/**
 * Utility class for links
 * @author vanderw
 *
 */
public class LinkUtil {
	


	private static final Logger logger = LogManager.getLogger(LinkUtil.class);
	
	
	public static final String _LINKS = "_links";
	public static final String LINK_REL = "rel";
	public static final String LINK_HREF = "href";
	
	
	
	/**
	 * Filters self from the links
	 * @param links a list of the links
	 * @return a list of filtered links
	 */
	public static List<Link> filterSelf( List<Link> links ) {
		List<Link> filteredList = new ArrayList<>();
		Link self = getSelf(links);
		if ( self == null ) {
			return links;
		}
		String selfHref = removeProjectionReference(self.getHref());
		for ( Link link : links ) {
			if ( !removeProjectionReference(link.getHref()).equals(selfHref)) {
				filteredList.add(link);
			}
		}
		return filteredList;
	}
	
	
	public static List<Link> removeLink( List<Link> links, String name ) {
		List<Link> filteredList = new ArrayList<>();
		for ( Link link : links ) {
			if ( !link.getRel().value().equals(name)) {
				filteredList.add(link);
			}
		}
		return filteredList;
	}
	
	
	
	/**
	 * Gets the self link
	 * @param links a list of links
	 * @return the link containing the self
	 */
	public static Link getSelf( List<Link> links) {
		if ( links != null ) {
			for ( Link link : links ) {
				if ( link.getRel().equals(IanaLinkRelations.SELF)) {
					return link;
				}
			}
		}
		else {
			logger.info("no self found, izzz null");
		}
		return null;
	}
	
	
	/**
	 * Gets the self
	 * @param model the entitymodel
	 * @return the Link identifying the self
	 */
	public static Link getSelf( EntityModel<?> model ) {
		for ( Link link : model.getLinks() ) {
			if ( link.getRel().equals(IanaLinkRelations.SELF)) {
				return link;
			}
		}
		return null;
	}
	
	
	public static String getSelfHref( EntityModel<?> model ) {
		for ( Link link : model.getLinks() ) {
			if ( link.getRel().equals(IanaLinkRelations.SELF)) {
				return link.getHref();
			}
		}
		return null;
	}
	
	public static String getSelfHref( Map entity ) {
		logger.info("entity {}", entity);
		Map<String,Map> links = (Map)entity.get(_LINKS);
		logger.info("links {}", links);
		if ( links.containsKey(IanaLinkRelations.SELF.toString())) {
			logger.info("contains");
			return (String)links.get(IanaLinkRelations.SELF.toString()).get(LINK_HREF);
		}
		return null;
	}
	
	
	
	/**
	 * Removes the projection reference fron the href ( {?projection}
	 * @param href the href given
	 * @return a string with projections removed
	 */
	public static String removeProjectionReference( String href ) {
		return href.replaceAll("\\{\\?projection\\}", "");
	}

}
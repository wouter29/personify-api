package be.personify.iam.api;

import org.springframework.hateoas.EntityModel;
import org.springframework.web.client.HttpStatusCodeException;

import be.personify.iam.api.util.Query;

public class APIException extends RuntimeException {
	
	private static final long serialVersionUID = -1442687753423872325L;
	
	
	private int code;
	
	private EntityModel<?> entityModel = null;
	
	private Query query = null;


	public APIException() {
		super();
	}
	
	
	public APIException ( HttpStatusCodeException httpStatusCodeException) {
		this(httpStatusCodeException, null, null);
	}
	
	public APIException ( HttpStatusCodeException httpStatusCodeException, EntityModel<?> entityModel) {
		this(httpStatusCodeException, entityModel, null);
	}
	
	public APIException ( HttpStatusCodeException httpStatusCodeException, EntityModel<?> entityModel, Query query) {
		super(httpStatusCodeException);
		this.code = httpStatusCodeException.getRawStatusCode();
		this.entityModel = entityModel;
		this.query = query;
	}

	
	

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public EntityModel<?> getEntityModel() {
		return entityModel;
	}


	public Query getQuery() {
		return query;
	}
	
	
	
	

}

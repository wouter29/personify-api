package be.personify.iam.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.hateoas.Link;

import com.fasterxml.jackson.databind.ObjectMapper;

import be.personify.iam.api.util.SearchResult;

public class SearchResultTest {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void testSearchResult() throws Exception {
		Map<String,Object> entity = new HashMap<>();
		entity.put("name", "johnny");
		Link link = Link.of("http://test", "self");
		SearchResult r = new SearchResult(entity, link);
		String s = mapper.writeValueAsString(r);
		
		System.out.println(s);
		
	}

}

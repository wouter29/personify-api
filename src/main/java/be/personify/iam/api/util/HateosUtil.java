package be.personify.iam.api.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.hateoas.Link;

public class HateosUtil {
	
	
	public static final String HREF = "href";
	
	public static final String EMBEDDED = "_embedded";
	
	public static final String LINKS = "_links";
	
	public static final String PAGE = "page"; 
	
	
	public static void addLinks(Map<String,Object> map, Link... links) {
		Map<String,Map<String,String>> linkMap = new HashMap<String,Map<String,String>>();
		if ( map.containsKey(LINKS)) {
			linkMap = (Map)map.get(LINKS);
		}
		for ( Link link : links ) {
			Map<String,String> hrefMap = new HashMap<String,String>();
			hrefMap.put(HateosUtil.HREF, link.getHref());
			linkMap.put(link.getRel().toString(),hrefMap);
		}
		map.put(HateosUtil.LINKS, linkMap);
	}

}

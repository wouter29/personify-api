package be.personify.iam.api;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.rest.webmvc.convert.UriListHttpMessageConverter;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
//import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.google.common.collect.ImmutableMap;

import be.personify.iam.api.util.LinkUtil;
import be.personify.iam.api.util.TokenResponse;
import be.personify.util.DateUtils;
import be.personify.util.StringUtils;
import be.personify.util.TokenType;
import be.personify.util.io.IOUtils;


public class APIBase<T> {
	

	public static final String ERROR_MESSAGE_TEMPLATE = "error {} {} {}";
		
	
	public static final String AUTHORIZATION = "Authorization";
	
	public static final String QUERY_PARAM_PAGE = "page";
	public static final String QUERY_PARAM_SIZE = "size";
	public static final String QUERY_PARAM_SORT = "sort";
	
	public static final String PLUS_REPLACEMENT = "%20";
	
	

	private static final Logger logger = LogManager.getLogger(APIBase.class);
	
	
	public static final ObjectMapper objectMapper = objectMapper();
	
	@Autowired
	private GenericTokenStore tokenStore; 
	
	/**
	 * Configure the objectmapper
	 * @return a ObjectMapper
	 */
	private static ObjectMapper objectMapper() {
	    ObjectMapper o = new ObjectMapper();
	    o.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	    o.setDateFormat(DateUtils.DATETIMEFORMAT_ISO);
	    o.enable(SerializationFeature.INDENT_OUTPUT);
	    o.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    o.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	    o.registerModule(new Jackson2HalModule());
	    return o;
	}
	
	

	/**
	 * Configure the message converters
	 * @param messageConverters the messageconverters needed
	 */
	private void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
	    messageConverters.add(new MappingJackson2HttpMessageConverter(objectMapper));
	    messageConverters.add(new FormHttpMessageConverter());
	    messageConverters.add(new UriListHttpMessageConverter());
	}
	
	
	public RestTemplate restTemplate() {
		return restTemplate(true);
	}
	
	
	public void setTokenStore( GenericTokenStore store ) {
		this.tokenStore = store;
	}
	
	 
	/**
	 * A new resttemplate
	 * @return the resttemplate composed
	 */
	
	/**
	 * A new resttemplate
	 * @param secured indicating a secured template
	 * @return the resttemplate composed
	 */
	public RestTemplate restTemplate(boolean secured ) {
	    RestTemplate restTemplate = new RestTemplate();
	    
	    HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory() {
	
    		@Override
    		public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
				ClientHttpRequest request = super.createRequest(uri, httpMethod);
				if ( secured ) {
					request.getHeaders().add(AUTHORIZATION, getToken());
				}
				return request;
			}
		    	
    	};
    	
    	restTemplate.setRequestFactory(httpComponentsClientHttpRequestFactory);
	        
	    List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
	    configureMessageConverters(messageConverters);
	    restTemplate.setMessageConverters(messageConverters);
	    return restTemplate;
	}
	
	
	/**
	 * Returns the token
	 * @return a string containing the token
	 */
	public String getToken() {
		TokenResponse token = tokenStore.getTokenResponse();
		if ( token != null) {
			return TokenType.Bearer + StringUtils.SPACE + token.getAccess_token();
		}
		return StringUtils.EMPTY_STRING;
	}
	
	
	/**
	 * A request
	 * @param URL the url for which to compose a request
	 * @return the requestentity to be returned
	 */
	public RequestEntity<Void> getRequest(String URL) {
		URL = URL.replaceAll("\\{\\?projection\\}", "");
		logger.debug("constructing getRequest for url [{}]", URL);
		URI uri = URI.create(URL);
		return RequestEntity.get(uri).accept(MediaTypes.HAL_JSON).acceptCharset(IOUtils.defaultCharset()).build();
	}

	
	public RequestEntity<Void> postRequest(String URL) {
		logger.debug("constructing postRequest for url [{}]", URL);
		URI uri = URI.create(URL);
		return RequestEntity.post(uri).accept(MediaTypes.HAL_JSON).acceptCharset(IOUtils.defaultCharset()).build();
	}
	
	
	public void processAssociations( EntityModel<?> sourceObject, EntityModel<?> savedObject ) {
		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.add(HttpHeaders.CONTENT_TYPE, new MediaType("text", "uri-list").toString());
		//logger.debug("source {}", sourceObject);
		for (Link link : LinkUtil.filterSelf(savedObject.getLinks().toList())) {
			//logger.debug("processing link {} {} {}", link.getHref(), link.getRel(), link.getName());
			Link newLink = null;
			try {
				newLink = sourceObject.getLink(link.getRel()).get();
			}
			catch ( Exception e ) {
				logger.debug("link not found in object {}", link.getRel());
				//e.printStackTrace();
			}
			if (newLink != null && !StringUtils.isEmpty(link.getHref()) ) {
				logger.debug("link found in object from form -> have to associate {}", newLink);
				
				try {
					
					logger.debug("newlink {} link {}", newLink, link.getHref());
					if ( !newLink.getHref().equals(link.getHref())) {
						HttpEntity<String> reqEntity = new HttpEntity<String>( newLink.getHref(), reqHeaders);
						try {
							//new resttemplate without converters!
							 RestTemplate restTemplate = new RestTemplate();
							 
							 restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory() {
									
								@Override
								public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
									ClientHttpRequest request = super.createRequest(uri, httpMethod);
									request.getHeaders().add(AUTHORIZATION, getToken());
									return request;
								}
							    	
						    });
							 
							ResponseEntity<String> res = restTemplate.exchange(LinkUtil.removeProjectionReference(link.getHref()), HttpMethod.PUT, reqEntity, String.class, ImmutableMap.of());
							logger.debug("linked object {}", res);
						}
						catch(HttpStatusCodeException e) {
					        logger.error("1 error {} {} {}", e.getRawStatusCode() , e.getResponseHeaders(), e.getResponseBodyAsString());
					    }
						catch( Exception e ) {
							logger.error("1 TODO already linked ? {}", e.getMessage());
						}
					}
					else {
						logger.debug("link is the same doing nothing");
					}

				} catch (Exception e) {
					e.printStackTrace();
					logger.error("can not", e);
				}
			}
		}
	}
	
	
	
	/**
	 * Deletes a ${entity.name} by href
	 * @param href the href given
	 */
	public void delete ( String href ) {
		logger.debug("deleting {}", href );
		long start = System.currentTimeMillis();
		restTemplate().delete(href);
		long end = System.currentTimeMillis();
		logger.info("deleting done in [{} ms]", (end - start));
	}
	
	
	/**
	 * Compose the uri components
	 * @param page the page
	 * @param size the size
	 * @param sort the sort
	 * @param searchCriteria the searchcriteria
	 * @param url the url
	 * @return the uricomponents
	 */
//	public UriComponents composeUriComponents(long page, long size, String sort, SearchCriteria searchCriteria, StringBuilder url) {
//		if (searchCriteria != null && !searchCriteria.getCriteria().isEmpty() ) {
//			url.append(StringUtils.QUESTION_MARK);
//			for ( SearchCriterium crit : searchCriteria.getCriteria()) {
//				url.append(crit.getKey())
//					.append(crit.getOperator().getValue())
//					.append(getMappedValue(crit.getValue()))
//					.append(StringUtils.AMPERSAND);
//			}
//			url.setLength(url.length() - 1);
//		}
//		else {
//			url.append(StringUtils.QUESTION_MARK).append(ONE).append(SearchCriteriaOperator.EQUALS.getValue()).append(ONE);
//		}
//		if ( size == 0 ) {
//			size=1000;
//		}
//		url.append(StringUtils.AMPERSAND).append(QUERY_PARAM_PAGE).append(SearchCriteriaOperator.EQUALS.getValue()).append(page);
//		url.append(StringUtils.AMPERSAND).append(QUERY_PARAM_SIZE).append(SearchCriteriaOperator.EQUALS.getValue()).append(size);
//		
//		if( !StringUtils.isEmpty(sort)) {
//			url.append(StringUtils.AMPERSAND).append(QUERY_PARAM_SORT).append(SearchCriteriaOperator.EQUALS.getValue()).append(sort);
//		}
//		
//		UriComponents uriComponents = UriComponentsBuilder.newInstance()
//			.uri(URI.create(url.toString()))
//			.queryParam(QUERY_PARAM_PAGE, page)
//			.queryParam(QUERY_PARAM_SIZE, size)
//			.queryParam(QUERY_PARAM_SORT, sort)
//			.build(true);
//
//		return uriComponents;
//	}
	
	
	
	
	
	/**
	 * compse the components
	 * @param page the page
	 * @param size the size
	 * @param sort the sort
	 * @param url the url
	 * @return the uricomponents
	 */
	public UriComponents composeUriComponents(long page, long size, String sort, StringBuilder url) {
		url.append(StringUtils.QUESTION_MARK);
		if ( size == 0 ) {
			size=1000;
		}
		url.append(QUERY_PARAM_PAGE).append(StringUtils.EQUALS).append(page);
		url.append(StringUtils.AMPERSAND).append(QUERY_PARAM_SIZE).append(StringUtils.EQUALS).append(size);
		
		if( !StringUtils.isEmpty(sort)) {
			url.append(StringUtils.AMPERSAND).append(QUERY_PARAM_SORT).append(StringUtils.EQUALS).append(sort);
		}
		
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
			.uri(URI.create(url.toString()))
			.queryParam(QUERY_PARAM_PAGE, page)
			.queryParam(QUERY_PARAM_SIZE, size)
			.queryParam(QUERY_PARAM_SORT, sort)
			.build(true);

		return uriComponents;
	}
	
	
	
	
	
	
	
	
}

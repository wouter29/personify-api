package be.personify.iam.api.util;

public enum QueryOperator {
	
	AND,
	OR;

}

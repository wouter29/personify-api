package be.personify.iam.api.util;

public enum CriteriumOperator {
	
	//order is important for looping and comparing
	
	NOT_EQUALS("!=",2),
	IN("<in>",2),
	EQUALS("=",2),
	LIKE("~",2),
	LESS_THEN_EQUALS("<=",2),
	GREATER_THEN_EQUALS(">=",2),
	LESS_THEN("<",2),
	GREATER_THEN(">",2),
	EMPTY(".empty()",1),
	NOT_EMPTY(".notEmpty()",1);
	
	
	private final String value;
	private final int numberOfParts;

	CriteriumOperator(final String newValue, final int nrOfparts) {
        value = newValue;
        numberOfParts = nrOfparts;
    }

    public String getValue() { return value; }
    
    public int getNumberOfParts() { return numberOfParts; }
	
	
}
